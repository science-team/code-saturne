Source: code-saturne
Section: science
Priority: optional
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Gilles Filippini <pini@debian.org>
Build-Depends: debhelper (>= 11), dh-exec, dh-python, python3-pyqt5,
 pyqt5-dev-tools, gfortran, autoconf,
# To build from upstream SVN source
  bison, flex,
# To help the configure detection on cs lib
  zlib1g-dev, libhdf5-mpi-dev (>= 1.10.0~), libcgns-dev (>= 3.3.0~), mpi-default-dev, libmedc-dev,
  libblas-dev | libatlas-dev, libopenmpi-dev,
  python3, libscotch-dev, tango-icon-theme
Build-Depends-Indep: doxygen, graphviz, fig2ps, texlive-latex-recommended, texlive-fonts-recommended, texlive-latex-extra
Standards-Version: 4.3.0
Homepage: http://www.code-saturne.org/
Vcs-Git: https://salsa.debian.org/science-team/code-saturne.git
Vcs-Browser: https://salsa.debian.org/science-team/code-saturne

Package: code-saturne
Priority: optional
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
 code-saturne-bin (= ${binary:Version}),
 code-saturne-data (= ${source:Version}),
 code-saturne-include (= ${binary:Version}), python3-pyqt5
Recommends: bash-completion, paraview, pdf-viewer, code-saturne-doc
Suggests: syrthes
Description: General purpose Computational Fluid Dynamics (CFD) software
 The basic capabilities of Code_Saturne enable the handling of either
 incompressible or expandable flows with or without heat transfer and
 turbulence. Dedicated modules are available for specific physics such
 as radiative heat transfer, combustion (gas, coal, heavy fuel oil, ...),
 magneto-hydrodynamics, compressible flows, two-phase flows
 (Euler-Lagrange approach with two-way coupling), extensions to
 specific applications (e.g. Mercure_Saturne for atmospheric
 environment).
 .
 It runs in parallel with MPI on distributed memory machines.
 Developed since 1997 at EDF R&D, it is based on a co-located Finite
 Volume approach that accepts meshes with any type of cell
 (tetrahedral, hexahedral, prismatic, pyramidal, polyhedral...) and any
 type of grid structure (unstructured, block structured, hybrid,
 conforming or with hanging nodes, ...).


Package: code-saturne-bin
Priority: optional
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, python3,
 libxml2-dev, libblas-dev
Recommends: code-saturne
Description: General purpose Computational Fluid Dynamics (CFD) software - binaries
 The basic capabilities of Code_Saturne enable the handling of either
 incompressible or expandable flows with or without heat transfer and
 turbulence. Dedicated modules are available for specific physics such
 as radiative heat transfer, combustion (gas, coal, heavy fuel oil, ...),
 magneto-hydrodynamics, compressible flows, two-phase flows
 (Euler-Lagrange approach with two-way coupling), extensions to
 specific applications (e.g. Mercure_Saturne for atmospheric
 environment).
 .
 It runs in parallel with MPI on distributed memory machines.
 Developed since 1997 at EDF R&D, it is based on a co-located Finite
 Volume approach that accepts meshes with any type of cell
 (tetrahedral, hexahedral, prismatic, pyramidal, polyhedral...) and any
 type of grid structure (unstructured, block structured, hybrid,
 conforming or with hanging nodes, ...).
 .
 This package contains the binary files.

Package: code-saturne-data
Priority: optional
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends}
Recommends: code-saturne
Description: General purpose Computational Fluid Dynamics (CFD) software - data
 The basic capabilities of Code_Saturne enable the handling of either
 incompressible or expandable flows with or without heat transfer and
 turbulence. Dedicated modules are available for specific physics such
 as radiative heat transfer, combustion (gas, coal, heavy fuel oil, ...),
 magneto-hydrodynamics, compressible flows, two-phase flows
 (Euler-Lagrange approach with two-way coupling), extensions to
 specific applications (e.g. Mercure_Saturne for atmospheric
 environment).
 .
 It runs in parallel with MPI on distributed memory machines.
 Developed since 1997 at EDF R&D, it is based on a co-located Finite
 Volume approach that accepts meshes with any type of cell
 (tetrahedral, hexahedral, prismatic, pyramidal, polyhedral...) and any
 type of grid structure (unstructured, block structured, hybrid,
 conforming or with hanging nodes, ...).
 .
 This package contains the data.

Package: code-saturne-include
Priority: optional
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends}, libcgns-dev, libmedc-dev,
 libhdf5-dev, zlib1g-dev, mpi-default-dev, libxml2-dev
Replaces: code-saturne-data (<< 3.3.2-3)
Breaks: code-saturne-data (<< 3.3.2-3)
Description: General purpose Computational Fluid Dynamics (CFD) software - includes
 The basic capabilities of Code_Saturne enable the handling of either
 incompressible or expandable flows with or without heat transfer and
 turbulence. Dedicated modules are available for specific physics such
 as radiative heat transfer, combustion (gas, coal, heavy fuel oil, ...),
 magneto-hydrodynamics, compressible flows, two-phase flows
 (Euler-Lagrange approach with two-way coupling), extensions to
 specific applications (e.g. Mercure_Saturne for atmospheric
 environment).
 .
 It runs in parallel with MPI on distributed memory machines.
 Developed since 1997 at EDF R&D, it is based on a co-located Finite
 Volume approach that accepts meshes with any type of cell
 (tetrahedral, hexahedral, prismatic, pyramidal, polyhedral...) and any
 type of grid structure (unstructured, block structured, hybrid,
 conforming or with hanging nodes, ...).
 .
 This package contains the include files.

Package: code-saturne-doc
Priority: optional
Section: doc
Architecture: all
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: General purpose Computational Fluid Dynamics (CFD) software - Documentation
 The basic capabilities of Code_Saturne enable the handling of either
 incompressible or expandable flows with or without heat transfer and
 turbulence. Dedicated modules are available for specific physics such
 as radiative heat transfer, combustion (gas, coal, heavy fuel oil, ...),
 magneto-hydrodynamics, compressible flows, two-phase flows
 (Euler-Lagrange approach with two-way coupling), extensions to
 specific applications (e.g. Mercure_Saturne for atmospheric
 environment).
 .
 It runs in parallel with MPI on distributed memory machines.
 Developed since 1997 at EDF R&D, it is based on a co-located Finite
 Volume approach that accepts meshes with any type of cell
 (tetrahedral, hexahedral, prismatic, pyramidal, polyhedral...) and any
 type of grid structure (unstructured, block structured, hybrid,
 conforming or with hanging nodes, ...).
 .
 This package contains the documentation.
