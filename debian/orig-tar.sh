#!/bin/sh
# Called from uscan with parameters:
# --upstream-version <release>
set -e

# We build the orig tarball from upstream SVN repository because their
# dist tarball includes many generated files:
# - doxygen docs
# - flex + bison C modules
# - gettext object files
# and misses some files needed to rebuild everything from source.

PACKAGE=code-saturne
UPSTREAM_VERSION="$2"
UPSTREAM_VERSION_MANGLED="$UPSTREAM_VERSION+repack"
UPSTREAM_TARBALL="../${PACKAGE}_$UPSTREAM_VERSION.orig.tar.gz"

SOURCE_DIR="code_saturne-$UPSTREAM_VERSION"
DEBIAN_SOURCE_DIR="$PACKAGE-$UPSTREAM_VERSION.orig"
REPACK_UPSTREAM_TARBALL="../${PACKAGE}_$UPSTREAM_VERSION_MANGLED.orig.tar.gz"

# Extract upstream tarball
tar xf "$UPSTREAM_TARBALL"

# rename upstream source dir
mv "$SOURCE_DIR" "$DEBIAN_SOURCE_DIR"

# remove empty directories
echo "Removing empty directories:"
find "$DEBIAN_SOURCE_DIR" -type d -empty -delete -print

# remove fig2dev generated pdf files
find "$DEBIAN_SOURCE_DIR"/docs -iname \*.pdf -exec sh -c 'file={} && [ -f ${file%.pdf}.fig ]' \; -delete -print

# repack into orig.tar.gz
GZIP=-9 tar -c -z -f "$REPACK_UPSTREAM_TARBALL" "$DEBIAN_SOURCE_DIR/"
rm -rf "$SOURCE_DIR" "$DEBIAN_SOURCE_DIR"

echo "$PACKAGE: downloaded upstream release $2 from SVN and renamed archive to $(basename "$REPACK_UPSTREAM_TARBALL")"
