#!/usr/bin/make -f

# Which MPI implementation?
# set ARCH_DEFAULT_MPI_IMPL
include /usr/share/mpi-default-dev/debian_defaults

DEB_CONFIGURE_USER_FLAGS := --with-mpi --libexecdir=/usr/lib/ --disable-rpath

ifneq ($(wildcard /usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/$(ARCH_DEFAULT_MPI_IMPL)/libhdf5.so),)
  DEB_CONFIGURE_USER_FLAGS += \
	--with-hdf5-include=/usr/include/hdf5/$(ARCH_DEFAULT_MPI_IMPL) \
	--with-hdf5-lib=/usr/lib/$(DEB_HOST_MULTIARCH)/hdf5/$(ARCH_DEFAULT_MPI_IMPL) \
	--with-mpi-include=/usr/include/$(DEB_HOST_MULTIARCH)/mpi
endif

# Force Python 3 and Qt 5
DEB_CONFIGURE_ENV := PYTHON=/usr/bin/python3 QT_SELECT=5

# Generate PDF files from .fig
# See docs/theory/graphics/README
FIG_FILES = $(wildcard docs/theory/graphics/*.fig)
PDF_FILES = $(FIG_FILES:.fig=.pdf)

# Generate Qt resources with QT_HASH_SEED=0 for reproducibility
export QT_HASH_SEED=0

%.pdf: %.fig
	fig2dev -L pdf $< $@

%:
	dh $@ -B_build

override_dh_auto_clean:
	dh_auto_clean
	rm -f debian/epoch.tex
	rm -f $(PDF_FILES)

override_dh_autoreconf:
	dh_autoreconf sbin/bootstrap

override_dh_auto_configure:
	$(DEB_CONFIGURE_ENV) dh_auto_configure -- $(DEB_CONFIGURE_USER_FLAGS)
	# Drop dpkg-buildflags from ple-config to improve reproducibility
	sed -i $(foreach flags,CFLAGS CXXFLAGS CPPFLAGS FCFLAGS LDFLAGS,-e '/^build_$(shell echo $(flags) | tr A-Z a-z)=/s/$(shell dpkg-buildflags --get $(flags) | sed 's:/:\\/:g')//') \
	  _build/libple/ple-config

override_dh_auto_build-arch:
	dh_auto_build

override_dh_auto_build-indep: override_dh_auto_build-arch $(PDF_FILES)
	# Force source date in pdf (instead of compile date)
	/bin/echo -e "$(shell date -d@$(SOURCE_DATE_EPOCH) "+\year=%Y\n\month=%m")" >debian/epoch.tex
	dh_auto_build -- doc

override_dh_auto_install-arch:
	dh_auto_install
	# Restore self.rpath in cs_config.py
	sed -i 's/self\.rpath = "" #/self.rpath =/' debian/tmp/usr/lib/python*/*-packages/code_saturne/cs_config.py
	# Drop dpkg-buildflags from cs_config.py
	sed -i $(foreach flags,CFLAGS CXXFLAGS CPPFLAGS FCFLAGS LDFLAGS,-e "/'$(shell echo $(flags) | tr A-Z a-z)':/s/$(shell dpkg-buildflags --get $(flags) | sed 's:/:\\/:g;s/^$$/^/')//") \
	  debian/tmp/usr/lib/python*/*-packages/code_saturne/cs_config.py

override_dh_auto_install-indep: override_dh_auto_install-arch
	$(MAKE) -C _build install-doc DESTDIR=$(CURDIR)/debian/tmp
	# Fix location of the ple doc and fix paths
	mv debian/tmp/usr/share/doc/ple/doxygen debian/tmp/usr/share/doc/code_saturne/doxygen/ple
	find debian/tmp/usr/share/doc/code_saturne/doxygen/src -type f \
	  -exec grep -q '/usr/share/doc/ple/doxygen' {} \; \
	  -exec sed -i 's:/usr/share/doc/ple/doxygen:/usr/share/doc/code-saturne/doxygen/ple:g' {} \; \
	  -print
	sed -i 's:$(CURDIR)/::g' debian/tmp/usr/share/doc/code_saturne/doxygen/ple/ple.tag

override_dh_install-arch:
	dh_install -a
	dh_python3 -pcode-saturne-include

override_dh_install-indep:
	# Exclude cs_config.py from package code-saturne-data:
	# It should go into code-saturne-include.
	# Exclude extra license file COPYING as well.
	dh_install -i -Xcs_config.py -XCOPYING
	chmod +x debian/code-saturne-data/usr/share/code_saturne/runcase*
	find debian/code-saturne-data -type f \( -name \*.pyc -o -name \*.pyo \) -delete
	dh_python3 -pcode-saturne-data
	# Remove internal doxygen files from code-saturne-doc
	find debian/code-saturne-doc/usr/share/doc/code-saturne/doxygen/src \
	  -type f \( -name _form\* -o -name \*.md5 -o -name \*.map \) -delete

override_dh_installdocs:
	dh_installdocs -A AUTHORS

override_dh_compress:
	dh_compress -X.pdf -X.xml -X.des -X.f90 -X.c -X.tag
