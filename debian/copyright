Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Code_Saturne
Upstream-Contact: Yvan Fournier <yvan.fournier@edf.fr>
Sources: https://github.com/code-saturne/code_saturne
Comment: Upstream tarball repacked to drop generated documentation (doxygen)
 and translation files. See debian/{watch,orig-tar.sh,orig-tar.exclude} for
 details.
License: GPL-2+
Copyright: 1998-2020 EDF

Files: * gui/*/icons/22x22/calc-mode.png gui/*/icons/22x22/MONO-bulle-HD.png gui/*/icons/22x22/prepro-mode.png gui/*/icons/22x22/text-xml.png
Copyright: 1998-2020 EDF
License: GPL-2+

Files: libple/*
Copyright: 2005-2020 EDF
License: LGPL-2.1+

Files: gui/*/icons/*/*.png salome/cfd_study/resources/edit-*.png
Copyright: Public Domain
License: public-domain
 These files come from the Tango icon theme, from the Tango Desktop Project, and are released to the Public Domain.
 See http://tango.freedesktop.org/Tango_Desktop_Project.

Files: debian/*
Copyright:
 2008-2014 Sylvestre Ledru <sylvestre.ledru@inria.fr>
 2014-2020 Gilles Filippini <pini@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.
